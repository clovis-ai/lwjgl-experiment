package fr.braindot.lwjgltest

import org.lwjgl.glfw.Callbacks.glfwFreeCallbacks
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11.*
import org.lwjgl.system.MemoryStack.stackPush
import org.lwjgl.system.MemoryUtil.NULL

fun main(args: Array<String>){
    println("Loading LWJGL...")

    Main.run()
}

object Main {

    val window: Long

    init {
        // Error callback
        GLFWErrorCallback.createPrint(System.err).set()

        // Initialisation
        if (!glfwInit())
            throw RuntimeException("Couldn't initialize GLFW...")

        // Configuration
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE)
        glfwWindowHint(GLFW_VISIBLE, GL_TRUE)

        // Create the window
        window = glfwCreateWindow(300, 300, "Test", NULL, NULL)
        if (window == NULL)
            throw RuntimeException("Couldn't create the window...")

        // What to do when a key is pressed
        glfwSetKeyCallback(window) { window, key: Int, scancode: Int, action: Int, mods: Int ->
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                glfwSetWindowShouldClose(window, true)
        }

        // Get the thread stack and push a new frame
        stackPush().use { stack ->
            val pWidth = stack.mallocInt(1) // int*
            val pHeight = stack.mallocInt(1) // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight)

            // Get the resolution of the primary monitor
            val vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor())

            // Center the window
            glfwSetWindowPos(
                window,
                (vidmode!!.width() - pWidth.get(0)) / 2,
                (vidmode.height() - pHeight.get(0)) / 2
            )
        } // the stack frame is popped automatically

        glfwMakeContextCurrent(window)
        glfwSwapInterval(1)
        glfwShowWindow(window)
    }

    fun run() {
        loop()

        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window)
        glfwDestroyWindow(window)

        // Terminate && free error callback
        glfwTerminate()
        glfwSetErrorCallback(null)?.free()
    }

    fun update() {
        glfwPollEvents()
    }

    fun render() {
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
        glfwSwapBuffers(window)
    }

    fun loop() {
        GL.createCapabilities()

        glClearColor(1.0f, 0.0f, 0.0f, 0.0f)

        while (!glfwWindowShouldClose(window)){
            update()
            render()
        }
    }

}